$(document).ready(function(){
    $('#btnAdd').click(function(){          
        var aTarefa ;
        if(window.localStorage['tarefa'] != ""){
            aTarefa = JSON.parse(window.localStorage['tarefa'] || '[]');

        }else{
            aTarefa = [];
        }

      
		        
        var oTarefa = {
            categoria: $('#categoria').val(),
            data: $('#data').val(),
            nmTarefa: $('#nmTarefa').val(),
			descTarefa: $('#descTarefa').val(),
			status: 'Pendente'
        };
        aTarefa.push(oTarefa);		
        window.localStorage['tarefa']= JSON.stringify(aTarefa);

        window.location.href = "listaTarefa.html";
    });
        
});

